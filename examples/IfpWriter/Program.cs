﻿using CommandLineParser;
using IndexedFilePackageLibrary;
using System;
using System.Collections.Generic;
using System.IO;

namespace IfpWriter
{
    static class Program
    {
        static void Main(string[] args)
        {
            var commandsErrs = new List<string>();
            var validator = new CommandLineValidator();
            validator.AcceptRawValueCount = int.MaxValue;
            validator.Options.Add(new CommandLineOption("base", 'b', true, false));
            validator.Options.Add(new CommandLineOption("path", 'P', true, false));
            validator.Options.Add(new CommandLineOption("pattern", 'p', true, false));
            validator.Options.Add(new CommandLineOption("out", 'o', true, false));
            validator.Options.Add(new CommandLineOption("useintfilesize", 'i', true, false));
            if (!validator.TryParseAndValidate(Environment.CommandLine, commandsErrs))
            {
                PrintUsage();
                foreach (var err in commandsErrs)
                {
                    Console.WriteLine(err);
                }
                return;
            }

            string basePath = validator.GetValue("base");
            string searchPath = validator.GetValue("path");
            string searchPattern = validator.GetValue("pattern");
            string outputFile = validator.GetValue("out");

            if (string.IsNullOrWhiteSpace(basePath))
            {
                PrintUsage();
                Console.WriteLine("base not specified.");
                return;
            }
            if (string.IsNullOrWhiteSpace(searchPath))
            {
                PrintUsage();
                Console.WriteLine("path not specified.");
                return;
            }
            if (string.IsNullOrWhiteSpace(searchPattern))
            {
                searchPattern = "*";
            }
            if (string.IsNullOrWhiteSpace(outputFile))
            {
                PrintUsage();
                Console.WriteLine("out not specified.");
                return;
            }

            RunApp(basePath, searchPath, searchPattern, outputFile, validator.Contains("useintfilesize"));
        }

        static void PrintUsage()
        {
            Console.WriteLine("Usage: --base basePath --path searchPath --pattern searchPattern --out outputFile --useintfilesize");
        }

        static void RunApp(string basePath, string searchPath, string searchPattern, string outputFile, bool useIntFileSize)
        {

            var builder = new IndexedFilePackageBuilder(basePath);
            builder.UseIntFileSize = useIntFileSize;
            builder.MaxItemCountPerNode = 127;
            builder.IndexNodePadding = 4096;
            builder.FileNodePadding = 4;
            foreach (var file in Directory.EnumerateFiles(searchPath, searchPattern, SearchOption.AllDirectories))
            {
                builder.Add(file);
            }
            if (File.Exists(outputFile))
            {
                File.Delete(outputFile);
            }
            builder.Save(outputFile);
        }
    }
}
