﻿namespace IndexedFilePackageLibrary.FileProvider
{
    public struct FileInPackage
    {
        public string PackagePath { get; private set; }
        public string FileName { get; private set; }

        public FileInPackage(string packagePath, string fileName)
        {
            PackagePath = packagePath;
            FileName = fileName;
        }
    }
}
