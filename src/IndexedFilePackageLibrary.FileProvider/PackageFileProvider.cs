﻿using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Primitives;
using System;

namespace IndexedFilePackageLibrary.FileProvider
{
    public delegate FileInPackage LocateFileInPackageDelegate(string path);
    public delegate FileInPackage LocateFileInPackageDelegateWithState(string path, object state);

    public class PackageFileProvider : IFileProvider
    {
        private readonly LocateFileInPackageDelegateWithState _locateDelegate;
        private readonly object _state;

        public PackageFileProvider(LocateFileInPackageDelegate locateDelegate)
        {
            if (locateDelegate == null)
            {
                throw new ArgumentNullException(nameof(locateDelegate));
            }
            _state = null;
            _locateDelegate = (path, state) => locateDelegate(path);
        }
        public PackageFileProvider(LocateFileInPackageDelegateWithState locateDelegate, object state)
        {
            if (locateDelegate == null)
            {
                throw new ArgumentNullException(nameof(locateDelegate));
            }
            _state = state;
            _locateDelegate = locateDelegate;
        }

        public IDirectoryContents GetDirectoryContents(string subpath)
        {
            return PackageDirectoryContents.Instance;
        }

        public IFileInfo GetFileInfo(string subpath)
        {
            FileInPackage location = _locateDelegate(subpath, _state);
            return new PackageFileInfo(subpath, location);
        }

        public IChangeToken Watch(string filter)
        {
            throw new NotSupportedException();
        }
    }
}
