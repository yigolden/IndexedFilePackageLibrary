﻿using Microsoft.Extensions.FileProviders;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace IndexedFilePackageLibrary.FileProvider
{
    class PackageDirectoryContents : IDirectoryContents
    {
        private static readonly PackageDirectoryContents s_instance = new PackageDirectoryContents();
        internal static PackageDirectoryContents Instance => s_instance;

        public bool Exists => false;

        public IEnumerator<IFileInfo> GetEnumerator()
        {
            return Enumerable.Empty<IFileInfo>().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Enumerable.Empty<IFileInfo>().GetEnumerator();
        }
    }
}
