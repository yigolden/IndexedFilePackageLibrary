﻿using System;

namespace IndexedFilePackageLibrary
{
    internal static class FileNameHelper
    {
        public static int CompareFileNameNoHash(ReadOnlySpan<byte> x, ReadOnlySpan<byte> y)
        {
            long xl = x.Length, yl = y.Length;
            for (int i = 0; i < xl && i < yl; i++)
            {
                if (x[i] > y[i])
                {
                    return 1;
                }
                else if (x[i] < y[i])
                {
                    return -1;
                }
            }
            if (xl > yl)
            {
                return 1;
            }
            else if (xl < yl)
            {
                return -1;
            }
            else
            {
                return 0;
            }
        }
    }
}
